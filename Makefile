EXTENSION = dejvice

DATA =	dejvice--1.0.0.sql \
	dejvice--1.0.0--1.0.1.sql

REGRESS = install_test \
	  data

PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
